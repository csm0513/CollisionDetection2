#! /usr/bin/env python

import os
import random
import pygame
import unittest
import math
from Weather import *

# Colors
black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 25, 25)


class TrackSegmentFlaw():
    def __init__(self, segmentFlaws):
        self.segmentFlaws = segmentFlaws



class TrackSegmentExits():
    def __init__(self, segmentExits):
        self.segmentExits = segmentExits

class TrackSegment (pygame.sprite.Sprite):
    # This base class holds all the MUST information for one segment of track.
    # Image = image gif to use for the segment
    # Length = length in pixels of the segment
    # Width = width in pixels of the segment
    # Orientation = degrees rotated in the clockwise direction from 0 degrees being directly north/south (precisely vertical).
    # LocX = X coordinate of centerpoint of segment
    # LocY = Y coordinate of centerpoint of segment
    # Flaws = Flaws object. See Flaws class definition
    # TractionMultiplier = multiplier that will be applied to this segment while on this segment. 100 is neutral. Anything
    # greater than 100 is not allowed (will be altered to 100). Less than 100 increases the chances of traction loss.

    def __init__ (self, segmentImage, segmentLength, segmentWidth, segmentOrientation, segmentLocX, segmentLocY,
                  segmentFlaws, segmentTractionMultiplier):
        super().__init__()
        self.segmentImage = segmentImage
        self.segmentLength = segmentLength
        self.segmentWidth = segmentWidth
        self.segmentOrientation = segmentOrientation
        self.segmentLocX = segmentLocX
        self.segmentLocY = segmentLocY
        self.segmentFlaws = segmentFlaws
        self.segmentTractionMultiplier = segmentTractionMultiplier

        self.image = pygame.Surface([self.segmentLength, self.segmentWidth])
        self.image.fill(black)
        # Set our transparent color
        self.image.set_colorkey(white)

        # Load the image - ignoring for now. Will figure that out later.
        if self.segmentImage != "":
            self.image = pygame.image.load(self.segmentImage).convert()

        # Fetch the rectangle object that has the dimensions of the image image.
        # Update the position of this object by setting the values
        # of rect.x and rect.y
        self.rect = self.image.get_rect()

    def __str__(self):
        return "Segment Loc ("+str(self.segmentLocX)+","+str(self.segmentLocY)+"): Len = "+str(self.segmentLength)+" Width = "+str(self.segmentWidth)+"\n"+\
               "   Orientation = "+str(self.segmentOrientation)+" degrees  TractionMultiplier = "+str(self.segmentTractionMultiplier)+\
               "  Flaws = "+"<Not Yet Implemented>"

class IntersectionSegment (TrackSegment):
    # This class is for any segment that doesn't have exactly 2 exits that are directly opposite of each other.
    def __init__(self, segmentImage, segmentLength, segmentWidth, segmentOrientation, segmentLocX, segmentLocY,
                  segmentFlaws, segmentTractionMultiplier, intersectionExits):
        super().__init__(segmentImage, segmentLength, segmentWidth, segmentOrientation, segmentLocX, segmentLocY,
                  segmentFlaws, segmentTractionMultiplier)
        self.intersectionExits = intersectionExits.copy()

    def __str__ (self):
        return super().__str__()+"\n   Intersection Exits: "+str(self.intersectionExits)

    # Deal with lights, stop signs, etc here

class Test_TrackSegment(unittest.TestCase):
    def test_TrackSegment(self):
        # Interrogate Weather Singleton for conditions
        rainingState = RainingState("Heavy")
        weather = Weather(rainingState)

        mySeg = IntersectionSegment("", 5, 5, 90, 320, 200, "none", weather.getWeatherTractionMultiplier(), [33, 66, 99])
        self.assertEqual(mySeg.segmentTractionMultiplier, 0.8)
        print(mySeg)

### MAIN ###
if __name__ == '__main__':
    unittest.main()
