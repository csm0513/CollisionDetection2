#! /usr/bin/env python

import os
import random
import pygame
import abc
import unittest
import six

### Weather State Abstract Base Class
class WeatherState (metaclass=abc.ABCMeta):
    def __init__(self):
        self.severity = ""
        self.tractionMultiplier = 1.0

    #@abc.abstractmethod
    def setSeverity (self, severity):
        self.severity = severity

    def setTractionMultiplier (self, tM):
        self.tractionMultiplier = tM

    def getTractionMuliplier (self):
        return (self.tractionMultiplier)

    def __str__(self):
        return "WeatherState Severity = "+self.severity+"\n"

### Weather Base Class States
class RainingState (WeatherState):
    # For a Raining state, allow "Light" or "Heavy". No severity defaults the Raining State to "Light".
    def __init__(self, severity):
        super().__init__()
        self.setSeverity(severity)
        self.setTractionMultiplier(severity)

    def setTractionMultiplier(self, severity):
        if severity == "":
            super().setTractionMultiplier(.95)
        elif severity == "Light":
            super().setTractionMultiplier(.95)
        elif str(severity) == "Heavy":
            super().setTractionMultiplier(.8)
        else:
            raise RuntimeError("Invalid Rain State Weather Severity requested ("+str(severity)+"). Only Light or Heavy are allowed for Raining")

    def __str__ (self):
        return "Weather State = Raining\n" + super().__str__()

class SnowingState (WeatherState):
    # For a Snow state, allow "Light", "Medium" or "Heavy". No severity defaults the Snowing State to "Light".
    def __init__(self, severity):
        super().__init__()
        self.setSeverity(severity)
        self.setTractionMultiplier(severity)

    def setTractionMultiplier(self, severity):
        if severity == "":
            super().setTractionMultiplier(.9)
        elif severity == "Light":
            super().setTractionMultiplier(.9)
        elif severity == "Medium":
            super().setTractionMultiplier(.7)
        elif severity == "Heavy":
            super().setTractionMultiplier(.6)
        else:
            raise RuntimeError("Invalid Snowing State Weather Severity requested ("+str(severity)+"). Only Light, Medium or Heavy are allowed for Snowing")

    def __str__ (self):
        return "Weather State = Snowing\n" + super().__str__()

### Weather Singleton
class WeatherSingleton(type):
     __instance = None

     def __call__(cls, *args, **kwargs):
         if not WeatherSingleton.__instance:
             WeatherSingleton.__instance = super(WeatherSingleton, cls).__call__(*args, **kwargs)
         else:
             WeatherSingleton.__instance.weatherState = args[0] # Only one argument allowed for the weather object - WeatherState.
         return WeatherSingleton.__instance

@six.add_metaclass (WeatherSingleton)
class Weather (object):
    # Weather class must be called with a Weather State
    def __init__ (self, weatherState):
        #print ("Weather constructed\n")
        self.weatherState = weatherState

    def getWeatherTractionMultiplier(self):
        return self.weatherState.getTractionMuliplier()

class Test_WeatherStateClass(unittest.TestCase):
    def test_Weather (self):
        # Test Weather State
        wState = RainingState("Heavy")
        #print (wState)
        self.assertEqual(wState.getTractionMuliplier(), .8)

        #Test Weather Singleton
        wState = SnowingState("Light")
        #print(wState)
        weather = Weather(wState)
        self.assertEqual(weather.getWeatherTractionMultiplier(), .9)
        wState = SnowingState("Heavy")
        #print(wState)
        weather1 = Weather(wState)
        self.assertEqual(weather1.getWeatherTractionMultiplier(), .6)
        self.assertNotEqual(weather.getWeatherTractionMultiplier(), .9)  # Since Weather is a Singleton. This should fail.

### MAIN ###
#if __name__ == '__main__':
#    unittest.main()