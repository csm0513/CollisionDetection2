#! /usr/bin/env python

import os
import random
import pygame

# Colors
black = (0,0,0)
white = (255,255,255)
red = (255,25,25)


class Car (pygame.sprite.Sprite):

    def __init__(self,image,width,height, x, y):
        super().__init__()
        self.image = pygame.Surface([width, height])
        self.image.fill(black)
        # Set our transparent color
        self.image.set_colorkey(white)

        # Load the image - ignoring for now. Will figure that out later.
        #self.image = pygame.image.load(image).convert()

        # Fetch the rectangle object that has the dimensions of the image image.
        # Update the position of this object by setting the values
        # of rect.x and rect.y
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

        # Initialize my Velocity
        self.velocity = self.Velocity()
        self.velocity.setV(0,0)

    class Velocity:
        def __init__(self):
            self.x=0
            self.y=0

        def setV (self, x,y):
            self.x = x
            self.y = y

    def setVelocity (self, velX, velY):
        self.velocity.setV (velX, velY)

    def detectFutureCollisionswithWalls(self, myWall):
        # Future Self is me after the next move
        futureSelf = Car("C:/Users/chmiller/Pictures/top-car-png-19.jpg",carWidth,carHeight, self.rect.x, self.rect.y)
        #futureSelf.rect = self.rect.copy()
        futureSelf.rect.x += self.velocity.x
        futureSelf.rect.y += self.velocity.y

        if futureSelf.rect.colliderect(myWall.rect):
            return True
        else:
            return False

    def render(self):
        # Remember, I'm guessing the render takes significant time and I'm calling this every cycle and for each wall.
        # For this reason, I want to minimize the work done in this call.

        if self.velocity.x != 0 or self.velocity.y != 0 :
            self.rect.x += self.velocity.x
            self.rect.y += self.velocity.y
            pygame.draw.rect(screen,white,(self.rect.x,self.rect.y,self.rect.width,self.rect.height))
        else: # We would hit the wall if we moved forward
            pygame.draw.rect(screen, red, (self.rect.x, self.rect.y, self.rect.width, self.rect.height))

    def turnMe (self, myWalls):
        # For now in this contrived example, I know I only have right, 90 degree turns to make so it's a pretty easy decision... For now.
        # Hmmmm, will need to figure this out. Only reliable with 90 degree turns.
        # Doing it this way so we only have to go through the Walls one time.
        foundCollisionAbove = False
        foundCollisionBelow = False
        foundCollisionLeft = False
        foundCollisionRight = False

        # We now need to figure out which direction is open. Take the first opening we find.
        if self.velocity.x != 0: # If I was traveling on the x axis
            # Check for collisions with walls on the top first, then the bottom. Take the first turn available
            self.velocity.x = 0

            self.velocity.y = 2 # Check a little further out than typical in case we're on the far side of the lane.
            for myWall in myWalls:
                if (self.detectFutureCollisionswithWalls(myWall)):
                    foundCollisionBelow = True
                else:
                    self.velocity.y = -2 # Check above us
                    if (self.detectFutureCollisionswithWalls(myWall)):
                        foundCollisionAbove = True
            if (foundCollisionBelow == False): # Didn't find a collision below me, safe to turn in that direction.
                self.velocity.y = 2
            elif (foundCollisionAbove == False): # Didn't find a collision above me, safe to turn in that direction.
                self.velocity.y = -2
            else: # No safe direction to turn. Stop.
                self.velocity.y = 0

        elif self.velocity.y != 0 : # If I was traveling on the y axis.
            # Check for collisions with walls on the right first, then the left. Take the first turn available
            self.velocity.y = 0

            self.velocity.x = 2 # Check a little further out than typical in case we're on the far side of the lane.
            for myWall in myWalls:
                if (self.detectFutureCollisionswithWalls(myWall)):
                    foundCollisionRight = True
                else:
                    self.velocity.x = -2 # Check above us
                    if (self.detectFutureCollisionswithWalls(myWall)):
                        foundCollisionLeft = True
            if (foundCollisionRight == False):  # Didn't find a collision to the right, safe to turn in that direction.
                self.velocity.x = 2
            elif (foundCollisionLeft == False):  # Didn't find a collision to the left, safe to turn in that direction.
                self.velocity.x = -2
            else: # No safe direction to turn. Stop.
                self.velocity.x = 0
        else:
            #Whoa, what happened?
            raise ("Error: Velocity 0 in both directions entering turn!")
        # print (foundCollisionAbove," ",foundCollisionBelow," ", foundCollisionLeft," ",foundCollisionRight)

    def moveMe (self, myWalls):
        for wall in myWalls:
            # Check all the walls to see if I will collide with them if I move forward. Is there a way to reduce the
            # number of walls I have to check here?
            if self.detectFutureCollisionswithWalls(wall):
                # Ack, if I do this move, I'll crash into a wall! Better turn!
                # Can I be more efficient here? Rather that check all the walls in the list again?
                self.turnMe(myWalls)
                break

        self.render()

# Nice class to hold a wall rect
class Wall(pygame.sprite.Sprite):

    def __init__ (self, color, width, height, x, y):
        super().__init__()
        self.image = pygame.Surface([width, height])
        self.image.fill(color)

        # Set our transparent color
        self.image.set_colorkey(white)

        # Fetch the rectangle object that has the dimensions of the image image.
        # Update the position of this object by setting the values
        # of rect.x and rect.y
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


#####################################################################################################################

# Initialize the number of text rows in the Walls text array.
#textrows = 18
textrows = 18

# Initialise pygame
os.environ["SDL_VIDEO_CENTERED"] = "1"
pygame.init()

# Set up the display
pygame.display.set_caption("Vroom, vroom!")
screen_width = 700
screen_height = 400
carWidth = 20
carHeight = 16
wallWidth = 20
wallHeight = 20
screen = pygame.display.set_mode((screen_width, screen_height))

clock = pygame.time.Clock()
wallsGroup = pygame.sprite.Group()
carsGroup = pygame.sprite.Group()

#walls = []  # List to hold the walls
car = Car("C:/Users/chmiller/Pictures/top-car-png-19.jpg",carWidth,carHeight, 32, 32)     # Create the first car
carsGroup.add (car)
# Get things running for the sake of example. +2 on the X axis
car.setVelocity(2,0)

# Holds the level layout in a list of strings.
level = [
    "WWWWWWWWWWWWWWWWWWWW",
    "W                  W",
    "W                  W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W                  W",
    "W                  W",
    "WWWWWWWWWWWWWWWWWWWW",
]

# Parse the level string above. W = wall, E = exit
x = y = 0
for row in level:
    for col in row:
        if col == "W":
            wall = Wall(red, wallWidth, wallHeight, x, y)
            wallsGroup.add(wall)
        elif col == " ":
            wall = Wall(white, wallWidth, wallHeight, x, y)
            wallsGroup.add(wall)
        x += textrows
    y += textrows
    x = 0


running = True
# Define which direction to start
#key = pygame.key.get_pressed()
#if key[pygame.K_LEFT]:
#   car.moveMe(-2, 0)
#if key[pygame.K_RIGHT]:
#  car.moveMe(2, 0)
#if key[pygame.K_UP]:
#  car.moveMe(0, -2)
#if key[pygame.K_DOWN]:
#  car.moveMe(0, 2)

while running:

    clock.tick(60)

    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            running = False
        if e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE:
            running = False

    # Draw the scene
    screen.fill((white))
    wallsGroup.draw(screen)
    carsGroup.draw(screen)

    # Now move the car
    #car.moveMe(wallsGroup)

    pygame.display.flip()