#! /usr/bin/env python

import os
import random
import pygame

# Colors
black = (0,0,0)
white = (255,255,255)
red = (255,25,25)

class Car:

    def __init__(self,x,y,width,heigth):
        self.rect = pygame.Rect(x,y,width,heigth)
        self.velocity = self.Velocity()
        self.velocity.setV(0,0)

    class Velocity:
        def __init__(self):
            self.x=0
            self.y=0

        def setV (self, x,y):
            self.x = x
            self.y = y

    def setVelocity (self, velX, velY):
        self.velocity.setV (velX, velY)

    def detectFutureCollisionswithWalls(self, myWall):
        # Future Self is me after the next move
        futureSelf = Car(32,32,16,16)
        futureSelf.rect = self.rect.copy()
        futureSelf.rect.x += self.velocity.x
        futureSelf.rect.y += self.velocity.y

        if futureSelf.rect.colliderect(myWall.rect):
            return True
        else:
            return False

    def render(self):
        # Remember, I'm guessing the render takes significant time and I'm calling this every cycle and for each wall.
        # For this reason, I want to minimize the work done in this call.

        if self.velocity.x != 0 or self.velocity.y != 0 :
            self.rect.x += self.velocity.x
            self.rect.y += self.velocity.y
            pygame.draw.rect(screen,white,(self.rect.x,self.rect.y,self.rect.width,self.rect.height))
        else: # We would hit the wall if we moved forward
            pygame.draw.rect(screen, red, (self.rect.x, self.rect.y, self.rect.width, self.rect.height))

    def turnMe (self, myWalls):
        # For now in this contrived example, I know I only have right, 90 degree turns to make so it's a pretty easy decision... For now.
        # Hmmmm, will need to figure this out. Only reliable with 90 degree turns.
        # Doing it this way so we only have to go through the Walls one time.
        foundCollisionAbove = False
        foundCollisionBelow = False
        foundCollisionLeft = False
        foundCollisionRight = False

        # We now need to figure out which direction is open. Take the first opening we find.
        if self.velocity.x != 0: # If I was traveling on the x axis
            # Check for collisions with walls on the top first, then the bottom. Take the first turn available
            self.velocity.x = 0

            self.velocity.y = 2 # Check a little further out than typical in case we're on the far side of the lane.
            for myWall in myWalls:
                if (self.detectFutureCollisionswithWalls(myWall)):
                    foundCollisionBelow = True
                else:
                    self.velocity.y = -2 # Check above us
                    if (self.detectFutureCollisionswithWalls(myWall)):
                        foundCollisionAbove = True
            if (foundCollisionBelow == False): # Didn't find a collision below me, safe to turn in that direction.
                self.velocity.y = 2
            elif (foundCollisionAbove == False): # Didn't find a collision above me, safe to turn in that direction.
                self.velocity.y = -2
            else: # No safe direction to turn. Stop.
                self.velocity.y = 0

        elif self.velocity.y != 0 : # If I was traveling on the y axis.
            # Check for collisions with walls on the right first, then the left. Take the first turn available
            self.velocity.y = 0

            self.velocity.x = 2 # Check a little further out than typical in case we're on the far side of the lane.
            for myWall in myWalls:
                if (self.detectFutureCollisionswithWalls(myWall)):
                    foundCollisionRight = True
                else:
                    self.velocity.x = -2 # Check above us
                    if (self.detectFutureCollisionswithWalls(myWall)):
                        foundCollisionLeft = True
            if (foundCollisionRight == False):  # Didn't find a collision to the right, safe to turn in that direction.
                self.velocity.x = 2
            elif (foundCollisionLeft == False):  # Didn't find a collision to the left, safe to turn in that direction.
                self.velocity.x = -2
            else: # No safe direction to turn. Stop.
                self.velocity.x = 0
        else:
            #Whoa, what happened?
            raise ("Error: Velocity 0 in both directions entering turn!")
        print (foundCollisionAbove," ",foundCollisionBelow," ", foundCollisionLeft," ",foundCollisionRight)

    def moveMe (self, myWalls):
        for wall in myWalls:
            # Check all the walls to see if I will collide with them if I move forward. Is there a way to reduce the
            # number of walls I have to check here?
            if self.detectFutureCollisionswithWalls(wall):
                # Ack, if I do this move, I'll crash into a wall! Better turn!
                # Can I be more efficient here? Rather that check all the walls in the list again?
                self.turnMe(myWalls)
                break

        self.render()

# Nice class to hold a wall rect
class Wall(object):

    def __init__(self, pos):
        walls.append(self)
        self.rect = pygame.Rect(pos[0], pos[1], textrows, textrows)


# Initialize the number of text rows in the Walls text array.
textrows = 18

# Initialise pygame
os.environ["SDL_VIDEO_CENTERED"] = "1"
pygame.init()

# Set up the display
pygame.display.set_caption("Vroom, vroom!")
screen = pygame.display.set_mode((350, 300))

clock = pygame.time.Clock()
walls = []  # List to hold the walls
car = Car(32,32,16,16)     # Create the first car
# Get things running for the sake of example. +2 on the X axis
car.setVelocity(2,0)

# Holds the level layout in a list of strings.
level = [
    "WWWWWWWWWWWWWWWWWWWW",
    "W                  W",
    "W                  W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W   WWWWWWWWWWWW   W",
    "W                  W",
    "W                  W",
    "WWWWWWWWWWWWWWWWWWWW",
]

# Parse the level string above. W = wall, E = exit
x = y = 0
for row in level:
    for col in row:
        if col == "W":
            Wall((x, y))
        x += textrows
    y += textrows
    x = 0
end_rect = pygame.Rect(x, y, textrows, textrows)

running = True
# Define which direction to start
key = pygame.key.get_pressed()
if key[pygame.K_LEFT]:
   car.moveMe(-2, 0)
if key[pygame.K_RIGHT]:
  car.moveMe(2, 0)
if key[pygame.K_UP]:
  car.moveMe(0, -2)
if key[pygame.K_DOWN]:
  car.moveMe(0, 2)

while running:

    clock.tick(60)

    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            running = False
        if e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE:
            running = False

    # Move the player if an arrow key is pressed
    # key = pygame.key.get_pressed()
    #if key[pygame.K_LEFT]:
    #    player.move(-2, 0)
    #if key[pygame.K_RIGHT]:
     #   player.move(2, 0)
    #if key[pygame.K_UP]:
    #    player.move(0, -2)
    #if key[pygame.K_DOWN]:
     #   player.move(0, 2)

    # Just added this to make it slightly fun ;)
    # if player.rect.colliderect(end_rect):
    #    raise SystemExit("You win!")

    # Draw the scene
    screen.fill((0, 0, 0))
    for wall in walls:
        pygame.draw.rect(screen, (255, 255, 255), wall.rect)
    #pygame.draw.rect(screen, (255, 0, 0), end_rect)

    # Now move the car
    car.moveMe(walls)

    pygame.display.flip()